This application process 10000 price records using NodeJS Cluster module.

#Installation

##Git clone or just download project files: 
### `git clone https://comprimator@bitbucket.org/comprimator/fincom.git`

##Install application running following commands in app directory:
### `cd fincom`
### `npm install`

##Build application
### 
### `npm run build`


# Start application
In the project directory, you can run:
### `npm run all`
