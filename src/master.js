/**
 * Master process
 * @type {module:cluster}
 */

import cluster from 'cluster';
import {cpus} from 'os';

const numCPUs = cpus().length;
const RECORDS_NUM = 10000;
const resultPrices = [];

/**
 * Configure cluster
 */
cluster.setupMaster({
  exec: 'src/worker.js',
});

/**
 * Generate random integer between 0 and max
 * @param max upped random limit
 * @returns {number} generated number
 */
function getRandomInt (max) {
  return Math.floor(Math.random() * Math.floor(max));
}

/**
 * Generate array of price records
 */
let generatePrices = async function () {
  return new Promise((resolve) => {
    let prices = [];
    for (let i = 0; i < RECORDS_NUM; i++) {
      prices.push({price: getRandomInt(1000)});
    }
    resolve(prices)
  });
};

/**
 * Process record
 * @param message
 */
const handleResult = message => {
  console.log(`Message from worker: ${JSON.stringify(message)}`);

  //putting processed record to output array in place
  if (message.type === 'RESULT') {
    resultPrices[message.index] = message.value;
  }
  //printing results when all elements in place
  if (resultPrices.filter(Boolean).length === RECORDS_NUM) {
    console.log('-------------------PROCESSED DATA----------------');
    console.log(JSON.stringify(resultPrices));
  }
};

let createWorker = async function () {
  return new Promise((resolve) => {
    const worker = cluster.fork();
    worker.on('message', handleResult);
    worker.on('error', error => console.log(error));
    worker.on('exit', (code, signal) => {
      if (code !== 0) {
        console.log(`Worker stopped with exit code ${code}, signal ${signal}`);
      }
    });
    resolve(worker);
  });
};

/**
 * Fork workers
 */
const startWorkers = async function () {
  return new Promise(async resolve => {
    for (let i = 0; i < numCPUs; i++) {
      await createWorker();
    }
    resolve();
  });
};

/**
 * Select a Worker
 * @returns {Worker | undefined}
 */
const getRandomWorker = function () {
  const workerIds = Object.keys(cluster.workers);
  const randomWorkerId = workerIds[getRandomInt(workerIds.length)];

  return cluster.workers[randomWorkerId];
};

/**
 * Send data to processing
 */
const sendDataToWorkers = async function (prices) {
  return new Promise(resolve => {
    for (let i = 0; i < prices.length; i++) {
      const worker = getRandomWorker();
      worker.send({type: 'PROCESS', index: i, value: prices[i]});
    }
    resolve();
  });
};

cluster.on('fork', (worker) => {
  console.log(`worker ${worker.process.pid} forked`);
});

cluster.on('exit', (worker, code, signal) => {
  console.log(`worker ${worker.process.pid} died. Code: ${code}, signal: ${signal}`);
});

/**
 * Main process
 */
(async () => {
  console.log(`Master ${process.pid} is running`);
  let prices = await generatePrices();
  await startWorkers();
  await sendDataToWorkers(prices);
})();