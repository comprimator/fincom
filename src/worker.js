/**
 * Worker process
 */
process.on('message', msg => {
  console.log(`Worker received message: ${JSON.stringify(msg)}`);

  //process record and sends result to Master
  process.send({type: 'RESULT', index: msg.index, value: {price: msg.value.price + 10}});

});